from django.contrib import admin
from django.urls import path
from .views import index, cari

urlpatterns = [
    path('', index, name='index'),
    path('cari', cari),
]