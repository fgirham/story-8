from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.

class Story8UnitTest(TestCase):

    def test_ada_url_story8(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_story8_pakai_fungsi_index(self):
        found = resolve('/')
        self.assertEqual(found.func, index)
    
    def test_template_yang_digunakan_benar(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'story8.html')

    def test_ada_tulisan_daftar_buku(self):
        response = Client().get('')
        self.assertContains(response, 'Daftar Buku')

    def test_ada_form_search(self):
        response = Client().get('')
        self.assertContains(response, 'Cari buku favoritmu!')

    def test_ada_button_search(self):
        response = Client().get('')
        self.assertContains(response, 'Cari</button>')

    def test_ada_tabel(self):
        response = Client().get('')
        self.assertContains(response, '</table>')

    def test_fungsi_cari_jalan(self):
        response_get = Client().get('/cari', {'key': 'a'})
        self.assertEqual(response_get.status_code, 200)

class Story8FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story8FunctionalTest, self).setUp()


    def tearDown(self):
        self.selenium.quit()
        super(Story8FunctionalTest, self).tearDown()

    def test_cari_buku_di_page(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')

        searchbox = selenium.find_element_by_id("search")
        searchbox.send_keys("Man")

        time.sleep(2)
        
        btnSearch = selenium.find_element_by_id("button")
        btnSearch.click()

        time.sleep(2)

        self.assertIn('The Descent of man', selenium.page_source)
