$(document).ready(() => {
    $('#search').val('')
    $.ajax({
        method: 'GET',
        url: 'cari?key=percy+jackson',
        success: function (response) {
            let bookList = $('tbody')
            bookList.empty()
            let rakBuku = response.items
            
            for (let i = 0; i < response.items.length; i++) {
                let book = rakBuku[i].volumeInfo

                if ('imageLinks' in book == false)
                    var img = $('<td>').text("-");
                else {
                    if ('smallThumbnail' in book.imageLinks == false)
                        var img = $('<td>').append($('<img>').attr({
                            'src': book.imageLinks.thumbnail
                        }));
                    else
                        var img = $('<td>').append($('<img>').attr({
                            'src': book.imageLinks.smallThumbnail
                        }));
                }
                var name = $('<td>').text(book.title);

                if ('authors' in book == false) var authors = $('<td>').text("-");
                else var authors = $('<td>').text(book.authors);

                var tr = $('<tr>').append(img, name, authors);

                $('tbody').append(tr);

            }


        }

    })
    $('#button').click(function () {
        var key = $('#search').val();

        $.ajax({
            method: 'GET',
            url: 'cari?key=' + key,
            success: function (response) {
                let bookList = $('tbody')
                bookList.empty()
                let rakBuku = response.items
                
                for (let i = 0; i < response.items.length; i++) {
                    let book = rakBuku[i].volumeInfo

                    var name = $('<td>').text(book.title);

                    if ('authors' in book == false) var authors = $('<td>').text("-");
                    else var authors = $('<td>').text(book.authors);

                    if ('imageLinks' in book == false)
                        var img = $('<td>').text("-");
                    else {
                        if ('smallThumbnail' in book.imageLinks == false)
                            var img = $('<td>').append($('<img>').attr({
                                'src': book.imageLinks.thumbnail
                            }));
                        else
                            var img = $('<td>').append($('<img>').attr({
                                'src': book.imageLinks.smallThumbnail
                            }));
                    }

                    var tr = $('<tr>').append(img, name, authors);

                    $('tbody').append(tr);

                }


            }

        })
    })
})
